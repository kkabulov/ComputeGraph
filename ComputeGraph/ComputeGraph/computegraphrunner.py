
import operations
from computegraph import ComputeGraph
from collections import Counter
import json

class ComputeGraphRunner(object):
    BLACK = 'black'
    GRAY = 'gray'

    ops_dict = {
        ComputeGraph.MAP : operations.map,
        ComputeGraph.SORT : operations.sort,
        ComputeGraph.FOLD : operations.fold,
        ComputeGraph.REDUCE : operations.reduce,
        ComputeGraph.JOIN : operations.join
    }

    def __init__(self, compute_graph):
        self.compute_graph = compute_graph
        # cache contains pairs id(graph):list(rows)
        self.cache = {}
        # node_children denotes how much children node has in topo order
        self.children_amount = Counter()
        # for dfs purposes
        self.topo_sort_used = {}
        # topologically sorted order, smaller index compute first
        self.topo_sort_order = []

    def run(self, output_file_name='output.txt'):
        last = None
        for cg in self.topo_sort():
            id_cg = id(cg)
            last = id_cg
            dispatch_output = self.dispatch_cg(cg, id_cg)
            self.save_to_cache(id_cg, dispatch_output)
        if last is None:
            raise Exception("happy debugging")
        self.dump_to_file(self.cache[last], output_file_name)

    def dispatch_cg(self, cg, id_cg):
        input = None
        for op in cg.get_operations():
            name = op[0]
            input = self.dispatch_and_run_operation(name, op[1:], input)
        return input

    def dispatch_and_run_operation(self, opname, params, generator):
        if opname == ComputeGraph.INPUT:
            yield from self.read_from_input_file(params[0])
        else:
            if generator is None:
                raise Exception('happy debugging')
            if opname not in ComputeGraphRunner.ops_dict:
                raise Exception('unsupported operation')
            elif opname == ComputeGraph.JOIN:
                yield from ComputeGraphRunner.ops_dict[opname](generator, self.cache[id(params[0])], *params[1:])
            else:
                yield from ComputeGraphRunner.ops_dict[opname](generator, *params)

    def read_from_input_file(self, file_name):
        lines = None
        with open(file_name, "r") as file:
            lines = file.readlines()
        for line in lines:
            yield json.loads(line)

    def dump_to_file(self, iterable, output_file_name):
        with open(output_file_name, 'w') as output_file:
            for elem in iterable:
                output_file.write(json.dumps(elem))
                output_file.write("\n")

    def save_to_cache(self, id_cg, generator):
        if self.children_amount[id_cg] > 1:
            self.cache[id_cg] = list(generator)
        else:
            self.cache[id_cg] = generator  # generator

    def topo_sort(self):
        # if the outer graph changes then data is incorrect here
        if self.topo_sort_order == []:
            cg = self.compute_graph
            self.topo_sort_dfs(cg, id(cg))
        return self.topo_sort_order

    def topo_sort_dfs(self, cg, cg_id):
        self.children_amount[cg_id] = 1
        self.topo_sort_used[cg_id] = ComputeGraphRunner.GRAY
        for operation in cg.get_operations():
            if operation[0] == ComputeGraph.JOIN:
                other = operation[1]
                other_id = id(other)
                if other_id not in self.topo_sort_used:
                    self.topo_sort_dfs(other, other_id)
                elif self.topo_sort_used[other_id] == ComputeGraphRunner.GRAY:
                    raise Exception("topo sort found cycle, tree like structure expected")
                elif self.topo_sort_used[other_id] == ComputeGraphRunner.BLACK:
                    self.children_amount[other_id] += 1
                else:
                    raise Exception('happy debugging')
        self.topo_sort_order.append(cg)
        self.topo_sort_used[cg_id] = ComputeGraphRunner.BLACK
