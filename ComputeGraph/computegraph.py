"""
    graph construction
    execution implemented in ComputeGraphRunner.py
"""

class ComputeGraph(object):
    """
    main methods:
        input - build read from input file
        map
        sort
        fold
        reduce
        join
    """

    INPUT='input'
    MAP='map'
    SORT='sort'
    FOLD='fold'
    REDUCE='reduce'
    JOIN='join'

    def __init__(self, filename):
        """

        :param filename: filename to read data from
        """
        self.input_file_name=filename
        new_elem = (ComputeGraph.INPUT, self.input_file_name)
        self.operations=[new_elem]

    # operations

    def map(self, mapper):
        """

        :param mapper:
        :return: ComputeGraph object with map step
        """
        new_elem = ComputeGraph.MAP, mapper
        self.operations.append(new_elem)
        return self

    def sort(self, keys):
        """

        :param keys: keys to use in sorting
        :return: ComputeGraph object with sort step
        """
        keys = self._make_tuple(keys)
        new_elem = ComputeGraph.SORT, keys
        self.operations.append(new_elem)
        return self

    def fold(self, folder, state):
        """

        :param folder: folder function
        :param state: initial state of folder
        :return: ComputeGraph object with fold step
        """
        new_elem = ComputeGraph.FOLD, folder, state
        self.operations.append(new_elem)
        return self

    def reduce(self, reducer, keys):
        """

        :param reducer: reducer function
        :param keys: input table must be sorted by keys
        :return: ComputeGraph object with reduce step
        """
        keys = self._make_tuple(keys)
        new_elem = ComputeGraph.REDUCE, reducer, keys
        self.operations.append(new_elem)
        return self

    def join(self, other, keys, strategy="inner", first_prefix=None, second_prefix=None):
        """

        :param other: other ComputeGraph object to combine results with
        :param keys: join by keys using different strategies
        :param strategy: inner, cross, outer (full | left | right)
        :param first_prefix: prefix for columns of self if there are equal columns in both tables
        :param second_prefix: prefix for columns of other
        :return: ComputeGraph object
        """
        if first_prefix == None:
            first_name = str(id(self)) + "_"
        if second_prefix == None:
            second_name = str(id(other)) + "_"
        keys = self._make_tuple(keys)
        # cloning here so that changes of other object do not affect self after joining
        other = other.clone()
        new_elem = ComputeGraph.JOIN, other, keys, strategy, first_prefix, second_prefix
        self.operations.append(new_elem)
        return self

    def clone(self, new_file_name=None):
        """
        if we change the old graph which is used somewhere everything crashes
        so clone it and do whatever with clone (append new commands)

        :param new_file_name: here we can craeate a ComputeGraph object with different input
        :return: ComputeGraph object
        """
        if new_file_name is None:
            new_file_name = self.input_file_name
        new_graph = ComputeGraph(new_file_name)
        new_graph.operations = self.operations[:]
        return new_graph

    def get_operations(self):
        """

        :return: operations list
        """
        return self.operations

    def _make_tuple(self, value):
        """

        :param value: tuples are used as keys everywhere,
        but for 1-elem keys we may have to convert it to tuple
        :return: tuple, if value is not tuple then make it tuple
        """
        if not isinstance(value, tuple):
            return (value,)
        return value

    # for debugging purposes
    def __repr__(self):
        """

        :return: operations list
        """
        string = ""
        for elem in self.operations:
            string += elem[0] + " "
        return string

