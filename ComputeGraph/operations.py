
# UTILITY stuff first

from operator import itemgetter
from itertools import groupby

def get_elem(row, keys):
    """

    :param row: dict object
    :param keys: desired keys set
    :return: tuple of values with keys from 'keys'
    """
    return itemgetter(*keys)(row)

def make_list(generator):
    """

    :param generator: generator object
    :return: list object
    """
    if not isinstance(generator, list):
        return list(generator)
    return generator

def is_sorted(array, keys):
    """

    :param array: list of dicts
    :param keys: compare dicts using only keys in 'keys'
    :return:
    """
    return all(get_elem(array[i], keys) <=
               get_elem(array[i + 1], keys)
               for i in range(len(array) - 1))

# OPERATIONS

def map(generator, mapper):
    """

    :param generator: list(generator) of dicts
    :param mapper: maps elements of generator to another list of dicts
    :return: yield
    """
    for elem in generator:
        yield from mapper(elem)

def sort(generator, keys_tuple):
    """

    :param generator: list(generator) of dicts
    :param keys_tuple: dicts are sorted by values of keys in 'keys'
    :return: yield
    """
    array = make_list(generator)
    if not is_sorted(array, keys_tuple):
        array = sorted(array, key=itemgetter(*keys_tuple))
    yield from array

def fold(generator, folder, state):
    """

    :param generator: list(generator) of dicts
    :param folder: folds 'generator' using state
    :param state: initial state of folder
    :return: yield
    """
    for elem in generator:
        state = folder(state, elem)
    yield state

def split_to_equal_parts(array, keys):
    """

    :param array: list of dicts
    :param keys: use 'keys' subset of keys
    :return:
    """
    result = []
    for k, g in groupby(array, itemgetter(*keys)):
        result.append(list(g))
    return result

def reduce(generator, reducer, keys):
    """

    :param generator: list(generator) of dicts
    :param reducer: reduce 'generator'
    :param keys: use 'keys' subset of keys
    :return: reduced list (return as generator)
    """
    array = make_list(generator)
    if not is_sorted(array, keys):
        raise Exception("reduce got not sorted sequence")
    array = split_to_equal_parts(array, keys)
    for item in array:
        yield from reducer(item)

# join FUNCTIONALITY
# TODO: generator is list here, rename

def assign_keys(keys, source=None, add_prefix_to_keys=""):
    result = {}
    if source is None:
        for key in keys:
            result[add_prefix_to_keys + key] = ""
    else:
        for key in keys:
            result[add_prefix_to_keys + key] = source[key]
    return result

def unite(first, second, first_prefix, second_prefix, is_left=False):
    """
    if is_left is True then forces "join outer left" in terms of sql

    :param is_left: outer (left|right) join may leave values of some cells as None
    but None is not comparable. So if is_left is True then set "" instead of None.

    other params described in join

    :return: return union of two dicts. collisions resolved by adding prefixes to keys
    """
    if second is None:
        yield first
    else:
        fst = set(first.keys())
        scd = set(second.keys())
        both = fst & scd
        result = {}
        result.update(assign_keys(fst - both, first))
        result.update(assign_keys(both, first, first_prefix))
        """
            below:
            if is_left is True then maps all keys of scd to ""
            otherwise adds (key, value) pairs of scd to result too
        """
        source = None if is_left else second
        result.update(assign_keys(scd - both, source))
        result.update(assign_keys(both, source, second_prefix))
        yield result

def join_cross(generator, other_generator, key, first_prefix, second_prefix):
    """
    params described in join
    :return: join cross
    """
    for first in generator:
        for second in other_generator:
            result = unite(first, second, first_prefix, second_prefix)
            yield from result

def join_inner(first, second, key, first_prefix, second_prefix):
    """
    params described in join
    :return: join inner
    """
    first = split_to_equal_parts(first, key)
    second = split_to_equal_parts(second, key)
    first_len = len(first)
    second_len = len(second)
    first_pos = 0
    second_pos = 0
    while first_pos < first_len and second_pos < second_len:
        first_block = first[first_pos]
        second_block = second[second_pos]
        first_val = get_elem(first_block[0], key)
        second_val = get_elem(second_block[0], key)
        if first_val < second_val:
            first_pos += 1
        elif first_val > second_val:
            second_pos += 1
        else:
            yield from join_cross(first_block, second_block, key, first_prefix, second_prefix)
            first_pos += 1
            second_pos += 1

# join outer FUNCTIONALITY

def join_left_only(generator, other_generator, key, first_prefix, second_prefix):
    """
    params described in join
    :return: join rows(dicts) of generator which are not in other_generator
    """
    for first in generator:
        flag = False
        for second in other_generator:
            fst_val = get_elem(first, key)
            scd_val = get_elem(second, key)
            flag = flag or (fst_val == scd_val)
        if not flag:
            if len(other_generator) == 0:
                yield from unite(first, None, first_prefix, second_prefix, True)
            else:
                yield from unite(first, other_generator[0], first_prefix, second_prefix, True)

def join_full_outer(generator, other_generator, key, first_prefix, second_prefix, is_left=True, is_right=True):
    """

    params described in join
    :param is_left: if True, adds rows(dicts) of generator which are not in other_generator to the result
    :param is_right: if True, adds rows(dicts) of other_generator which are not in generator to the result
    :return: join outer full
    """
    yield from join_inner(generator, other_generator, key, first_prefix, second_prefix)
    if is_left:
        yield from join_left_only(generator, other_generator, key, first_prefix, second_prefix)
    if is_right:
        yield from join_left_only(other_generator, generator, key, second_prefix, first_prefix)

def join_left_outer(generator, other_generator, key, first_prefix, second_prefix):
    """
    params described in join
    :return: join outer left
    """
    yield from join_full_outer(generator, other_generator, key, first_prefix, second_prefix, True, False)

def join_right_outer(generator, other_generator, key, first_prefix, second_prefix):
    """
    params described in join
    :return: join outer right
    """
    yield from join_left_outer(other_generator, generator, key, second_prefix, first_prefix)

class JoinStrategy(object):
    INNER = "inner"
    CROSS = "cross"
    FULL_OUTER = 'outer'
    LEFT_OUTER = 'left'
    RIGHT_OUTER = 'right'

    Strategy = {
        INNER: join_inner,
        CROSS: join_cross,
        FULL_OUTER: join_full_outer,
        LEFT_OUTER: join_left_outer,
        RIGHT_OUTER: join_right_outer
    }

def join(generator, other_generator, keys, strategy='inner', first_prefix=None, second_prefix=None):
    """

    :param generator: list(generator) of dicts
    :param other_generator: list(generator) of dicts
    :param keys: use 'keys' subset of keys
    :param strategy: inner, cross, outer (full | left | right)
    :param first_prefix: add this prefix to keys of generator
    if equal columns (keys) in generator and other_generator
    :param second_prefix: add this prefix to keys of other_generator
    :return: joined list(generator) {sorted by keys}
    """
    if strategy not in JoinStrategy.Strategy:
        raise Exception('unsupported strategy for joining')
    if strategy == 'cross':
        generator = make_list(generator)
        other_generator = make_list(other_generator)
    else:
        generator = list(sort(generator, keys))
        other_generator = list(sort(other_generator, keys))
    yield from JoinStrategy.Strategy[strategy](generator, other_generator, keys, first_prefix, second_prefix)

