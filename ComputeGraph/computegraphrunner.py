
import operations
from computegraph import ComputeGraph
from collections import Counter
import json

class ComputeGraphRunner(object):
    """
    implements execution of operations in ComputeGraph object
    """

    BLACK = 'black'
    GRAY = 'gray'

    _ops_dict = {
        ComputeGraph.MAP : operations.map,
        ComputeGraph.SORT : operations.sort,
        ComputeGraph.FOLD : operations.fold,
        ComputeGraph.REDUCE : operations.reduce,
        ComputeGraph.JOIN : operations.join
    }

    def __init__(self, compute_graph):
        self.compute_graph = compute_graph.clone()
        # cache contains pairs id(graph):list(rows)
        self.cache = {}
        # node_children denotes how much children node has in topo order
        self.children_amount = Counter()
        # for dfs purposes
        self.topo_sort_used = {}
        # topologically sorted order, smaller index - compute first
        self.topo_sort_order = []

    def run(self, output_file_name=None):
        """

        :param output_file_name: dump results to this file
        :return: nothing
        """
        id_cg = None
        for cg in self.topo_sort():
            id_cg = id(cg)
            dispatch_output = self._dispatch_cg(cg, id_cg)
            self._save_to_cache(id_cg, dispatch_output)
        return self.dump_to_file(self.cache[id_cg], output_file_name)

    def _dispatch_cg(self, cg, id_cg):
        """

        :param cg: ComputeGraph object
        :param id_cg: id of 'cg'
        :return: result of executing 'cg'
        list or generator (of dicts)
        """
        last = None
        for op in cg.get_operations():
            name, *params = op
            last = self._dispatch_and_run_operation(name, params, last)
        return last

    def _dispatch_and_run_operation(self, opname, params, generator):
        """

        :param opname: operation name
        :param params: operation parameters
        :param generator: input data(list of dicts)
        :return: result of applying operation 'opname' with parameters 'params' to 'generator'
        """
        if opname == ComputeGraph.INPUT:
            yield from self.read_from_input_file(params[0])
        else:
            if opname not in ComputeGraphRunner._ops_dict:
                raise Exception('unsupported operation')
            elif opname == ComputeGraph.JOIN:
                cg, *join_params = params
                yield from ComputeGraphRunner._ops_dict[opname](generator, self.cache[id(cg)], *join_params)
            else:
                yield from ComputeGraphRunner._ops_dict[opname](generator, *params)

    def read_from_input_file(self, file_name):
        """

        :param file_name: initial data (list of dicts)
        :return: list of dicts
        """
        with open(file_name, "r") as file:
            yield from map(json.loads, file.readlines())

    @staticmethod
    def dump_to_file(iterable, output_file_name=None):
        """

        :param iterable: list of dicts
        :param output_file_name: file to dump info
        :return: nothing
        """
        if output_file_name is None:
            return list(iterable)
        else:
            with open(output_file_name, 'w') as output_file:
                for elem in iterable:
                    output_file.write(json.dumps(elem))
                    output_file.write("\n")
            return None

    def _save_to_cache(self, id_cg, generator):
        """

        :param id_cg: id of ComputeGraph 'cg'
        :param generator: result of executing 'cg'
        :return: nothing
        """
        if self.children_amount[id_cg] > 1:
           self.cache[id_cg] = list(generator)
        else:
           self.cache[id_cg] = generator  # generator

    def topo_sort(self):
        """
        if the outer graph changes then data is incorrect here
        thus cloning is applied in constructor of ComputeGraphRunner and join
        :return: ComputeGraph object id`s in toposorted order
        smaller index means compute first (as results are used in following operations)
        """
        if self.topo_sort_order == []:
            cg = self.compute_graph
            self._topo_sort_dfs(cg, id(cg))
        return self.topo_sort_order

    def _topo_sort_dfs(self, cg, cg_id):
        """
        use dfs to build topological sort
        smaller index means compute first
        counts children amount too, if it is > 1 then it will be cached,
         to avoid computing again
        gray - in process now
        black - all parents visited

        :param cg: ComputeGraph object
        :param cg_id: id of 'cg'
        :return: nothing
        """
        self.children_amount[cg_id] = 1
        self.topo_sort_used[cg_id] = ComputeGraphRunner.GRAY
        for operation in cg.get_operations():
            if operation[0] == ComputeGraph.JOIN:
                other = operation[1]
                other_id = id(other)
                if other_id not in self.topo_sort_used:
                    self._topo_sort_dfs(other, other_id)
                elif self.topo_sort_used[other_id] == ComputeGraphRunner.GRAY:
                    raise Exception("topo sort found cycle, tree like structure expected")
                elif self.topo_sort_used[other_id] == ComputeGraphRunner.BLACK:
                    self.children_amount[other_id] += 1
                else:
                    raise Exception('happy debugging')
        self.topo_sort_order.append(cg)
        self.topo_sort_used[cg_id] = ComputeGraphRunner.BLACK
