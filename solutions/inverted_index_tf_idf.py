from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

from collections import Counter
import re
import math

# operations

def split_word_mapper(record):
    rec = record['text']
    txt = rec.split()
    for elem in txt:
        only_alnum = re.search('\w+', elem)
        if only_alnum is not None:
            only_alnum = only_alnum.group()
            if len(only_alnum) > 0:
                yield {
                    'word' : only_alnum,
                    'doc_id' : record['doc_id']
                }

def count_docs_folder(state, elem):
    state['docs_count'] += 1
    return state

def unique(rows):
    for elem in rows:
        yield elem
        break

def calc_idf(rows):
    for elem in rows:
        yield {
            "word" : elem['word'],
            "idf": math.log(elem['docs_count'] / len(rows))
        }
        break

def term_frequency_reducer(rows):
    if len(rows) == 0:
        raise Exception('cannot reduce empty list')
    word_count = Counter()
    doc_id = rows[0]['doc_id']
    for record in rows:
        word_count[record['word']] += 1
    total = sum(word_count.values())
    for word, count in word_count.items():
        yield {
            'doc_id': doc_id,
            'word' : word,
            'tf': count / total
        }

def invert_index(rows):
    docs_priority = []
    for record in rows:
        docs_priority.append((record['tf'] * record['idf'], record['doc_id']))
    docs_priority = sorted(docs_priority)
    docs_priority.reverse()
    if len(docs_priority) > 3:
        docs_priority = docs_priority[:3]
    word = rows[0]['calc_index_word']
    yield {
        'term': word,
        'index': list((item[1], item[0]) for item in docs_priority)
    }

# construct ComputeGraph

input_file_name = "text_corpus.txt"

count_docs = ComputeGraph(input_file_name) \
    .fold(count_docs_folder, {'docs_count':0})

count_idf = ComputeGraph(input_file_name) \
    .map(split_word_mapper) \
    .sort(('doc_id', 'word')) \
    .reduce(unique, keys=('doc_id', 'word')) \
    .join(count_docs, (), strategy='cross') \
    .sort('word') \
    .reduce(calc_idf, keys=('word'))

calc_index = ComputeGraph(input_file_name) \
    .map(split_word_mapper) \
    .sort('doc_id') \
    .reduce(term_frequency_reducer, keys=('doc_id')) \
    .join(count_idf,
          keys=('word'),
          strategy='inner',
          first_prefix='calc_index_',
          second_prefix='count_idf_') \
    .sort('calc_index_word') \
    .reduce(invert_index, keys=('calc_index_word'))

# execute graph

runner = ComputeGraphRunner(calc_index)
runner.run("output.txt")
