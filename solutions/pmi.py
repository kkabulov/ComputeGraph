
from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

from collections import Counter
import re
import math

input_file_name = "text_corpus.txt"

'''
    1. frequency of word in all words
'''

def text_to_words_mapper(record):
    for elem in record['text'].split():
        only_alnum = re.search('\w+', elem)
        if only_alnum is not None:
            only_alnum = only_alnum.group()
            if len(only_alnum) > 0:
                yield {
                    'word' : only_alnum,
                }

def total_words_count_folder(state, elem):
    state['total_words_count'] += 1
    return state

total_words_count = ComputeGraph(input_file_name) \
    .map(text_to_words_mapper) \
    .fold(total_words_count_folder, {'total_words_count':0})

#   ComputeGraphRunner(total_words_count).run("output.txt")

def one_word_count_reducer(rows):
    yield {
        'word' : rows[0]['word'],
        'count' : len(rows)
    }

one_word_counter = ComputeGraph(input_file_name) \
    .map(text_to_words_mapper) \
    .sort('word') \
    .reduce(one_word_count_reducer, keys='word')

#   ComputeGraphRunner(one_word_counter).run("output.txt")

def total_word_frequency_reducer(rows):
    yield {
        'word' : rows[0]['word'],
        'total_frequency' : rows[0]['count'] / rows[0]['total_words_count']
    }

total_word_frequency_counter = one_word_counter.clone() \
    .join(total_words_count, (), strategy='cross') \
    .sort('word') \
    .reduce(total_word_frequency_reducer, keys='word')

#   ComputeGraphRunner(total_word_frequency_counter).run("output.txt")

'''
    2. frequency of word in document
'''

def text_to_word_and_docid_mapper(record):
    for elem in record['text'].split():
        only_alnum = re.search('\w+', elem)
        if only_alnum is not None:
            only_alnum = only_alnum.group()
            if len(only_alnum) > 0:
                yield {
                    'word' : only_alnum,
                    'doc_id' : record['doc_id']
                }

def word_frequency_in_document_reducer(rows):
    word_count = Counter()
    for record in rows:
        word_count[record['word']] += 1
    total = sum(word_count.values())
    doc_id = rows[0]['doc_id']
    for word, count in word_count.items():
        yield {
            'doc_id': doc_id,
            'word' : word,
            'frequency': count / total
        }

word_frequency_in_document_counter = ComputeGraph(input_file_name) \
    .map(text_to_word_and_docid_mapper) \
    .sort('doc_id') \
    .reduce(word_frequency_in_document_reducer, keys=('doc_id'))

#   ComputeGraphRunner(word_frequency_in_document_counter).run("output.txt")

general_pmi = word_frequency_in_document_counter.clone() \
    .join(total_word_frequency_counter,
          keys='word',
          strategy='inner',
          first_prefix='',
          second_prefix='aux')

#   ComputeGraphRunner(general_pmi).run("output.txt")

'''
    3. now make table with following fields:
    word, word_in_docs, one_occurence_in_docs    
'''

def count_docs_folder(state, elem):
    state['docs_count'] += 1
    return state

count_docs = ComputeGraph(input_file_name) \
    .fold(count_docs_folder, {'docs_count':0})

def text_to_single_words_mapper(record):
    words = set()
    for elem in record['text'].split():
        only_alnum = re.search('\w+', elem)
        if only_alnum is not None:
            only_alnum = only_alnum.group()
            if len(only_alnum) > 0:
                words.add(only_alnum)
    for word in words:
        yield {
            'word' : word,
        }

def word_to_amount_reducer(words):
    occurence_count = len(words)
    yield {
        'word' : words[0]['word'],
        'occurence_indocs_count' : occurence_count
    }

count_word_occurences = ComputeGraph(input_file_name) \
    .map(text_to_single_words_mapper) \
    .sort('word') \
    .reduce(word_to_amount_reducer, 'word')

def text_to_set_of_once_words_mapper(record):
    ctr = Counter()
    for elem in record['text'].split():
        only_alnum = re.search('\w+', elem)
        if only_alnum is not None:
            only_alnum = only_alnum.group()
            if len(only_alnum) > 0:
                ctr[only_alnum] += 1
    for word, amount in ctr.items():
        if amount != 1:
            amount = 0
        yield {
            'word' : word,
            'is_only_one_occurence' : amount
        }

def word_to_one_occurence_amount_reducer(words):
    cnt = 0
    for word in words:
        cnt += word['is_only_one_occurence']
    yield {
        'word' : words[0]['word'],
        'one_occurence_amount' : cnt
    }

count_word_one_occurences = ComputeGraph(input_file_name) \
    .map(text_to_set_of_once_words_mapper) \
    .sort('word') \
    .reduce(word_to_one_occurence_amount_reducer, 'word')

predicate_graph = count_docs.clone() \
    .join(count_word_occurences,
          keys=(),
          strategy='cross') \
    .join(count_word_one_occurences,
          keys='word',
          strategy='inner',
          first_prefix='',
          second_prefix='aux')

'''
    4. now filter out words which meet following condition
    len(word) > 4, word_in_docs(word) == docs_count, one_occurence_in_docs(word) == 0
'''

def filter_mapper(record):
    word = record['word']
    one_occurence = record['one_occurence_amount']
    total_occurence = record['occurence_indocs_count']
    docs_count = record['docs_count']
    if len(word) > 4 and one_occurence == 0 and total_occurence == docs_count:
        yield record

def calc_pmi_mapper(record):
    yield {
        'doc_id' : record['doc_id'],
        'word' : record['word'],
        'pmi' : math.log(record['frequency'] / record['total_frequency'])
    }

def pmi_reducer(rows):
    words_priority = []
    for record in rows:
        words_priority.append((record['pmi'], record['word']))
    docs_priority = sorted(words_priority)
    docs_priority.reverse()
    if len(docs_priority) > 10:
        docs_priority = docs_priority[:10]
    doc_id = rows[0]['doc_id']
    yield {
        'doc_id': doc_id,
        'index': list((item[1], item[0]) for item in docs_priority)
    }

final_graph = general_pmi.clone() \
    .join(predicate_graph,
          keys='word',
          strategy='inner',
          first_prefix='',
          second_prefix='second') \
    .map(filter_mapper) \
    .map(calc_pmi_mapper) \
    .sort('doc_id') \
    .reduce(pmi_reducer, 'doc_id')

ComputeGraphRunner(final_graph).run("output.txt")
