from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner
import re

# operations

def mapper(record):
    rec = record['text']
    txt = rec.split()
    for elem in txt:
        only_alnum = re.search('\w+', elem)
        if only_alnum is not None:
            only_alnum = only_alnum.group()
            if len(only_alnum) > 0:
                yield {
                    'word' : only_alnum,
                }

def folder(state, record):
    word = record['word']
    if word not in state:
        state[word] = 0
    state[record['word']] += 1
    return state

# construct ComputeGraph

graph = ComputeGraph("text_corpus.txt") \
    .map(mapper) \
    .fold(folder, {})

# execute graph

runner = ComputeGraphRunner(graph)
runner.run("output.txt")
