import unittest

from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

def split_text_mapper(record):
    """
        split text into separate words
    """
    for word in record['body'].split():
        yield {
            'word': word,
            'id': record['id']
        }

def length_filter_mapper(record):
    """
        leave words with len > 5
    """
    if len(record['string']) > 5:
        yield record

class TestMap(unittest.TestCase):
    def test_split_text_mapper(self):
        """
            split text using ComputeGraph.map
        """
        records = [
            {"body": "hello world", "id": 1, "title": "python for dummies"},
            {"body": "kajetsa ya kogoto koknul", "id": 2, "title": "discrete maths"},
            {"body": "dont nobody go nowhere", "id": 3, "title": "back to the future"}
        ]

        ComputeGraphRunner.dump_to_file(records, "test_map.txt")

        graph = ComputeGraph("test_map.txt") \
            .map(split_text_mapper)

        result = ComputeGraphRunner(graph).run()

        for record in records:
            for word in record['body'].split():
                self.assertTrue(
                    any(
                        (row['id'], row['word']) == (record['id'], word)
                        for row in result
                    )
                )

    def test_length_filter_mapper(self):
        '''
            filter text using ComputeGraph.map
        '''

        records = [
            {"string": "one"},
            {"string": "two"},
            {"string": "terminator"},
            {"string": "warandpiece"},
            {"string": "monalisa"}
        ]

        ComputeGraphRunner.dump_to_file(records, "test_map.txt")

        graph = ComputeGraph("test_map.txt") \
            .map(length_filter_mapper)

        result = ComputeGraphRunner(graph).run()

        for record in result:
            self.assertGreater(len(record['string']), 5)

        self.assertEqual(len(result), 3)

if __name__ == '__main__':
    unittest.main()
