import unittest
from collections import Counter

from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

def split_text_mapper(record):
    """
        split text into separate words
    """
    for word in record['body'].split():
        yield {
            'word': word,
            'id': record['id']
        }

def term_frequency_reducer(rows):
    word_count = Counter()
    doc_id = rows[0]['id']
    for record in rows:
        word_count[record['word']] += 1
    total = sum(word_count.values())
    for word, count in word_count.items():
        yield {
            'id': doc_id,
            'word' : word,
            'tf': count / total
        }

class TestReduce(unittest.TestCase):
    def test_reduce(self):
        '''
            test ComputeGraph.reduce
        '''
        records = [
            {"body": "hello world", "id": 1, "title": "python for dummies"},
            {"body": "kajetsa ya kogoto koknul", "id": 2, "title": "discrete maths"},
            {"body": "able was i ere i saw elba", "id": 3, "title": "napoleon"}
        ]

        ComputeGraphRunner.dump_to_file(records, "test_reduce.txt")

        graph = ComputeGraph("test_reduce.txt") \
            .map(split_text_mapper) \
            .sort('id') \
            .reduce(term_frequency_reducer, 'id')

        result = ComputeGraphRunner(graph).run()

        for rec in result:
            if (rec['id'], rec['word']) == (1, 'hello'):
                self.assertEqual(rec['tf'], 0.5)
            if (rec['id'], rec['word']) == (2, 'ya'):
                self.assertEqual(rec['tf'], 0.25)
            if (rec['id'], rec['word']) == (3, 'i'):
                self.assertAlmostEqual(rec['tf'], 2 / 7, delta=10**(-10))

        self.assertEqual(12, len(result))

if __name__ == '__main__':
    unittest.main()
