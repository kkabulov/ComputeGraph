import unittest
from random import randint
from operator import itemgetter

from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

class TestSort(unittest.TestCase):
    def test_sort(self):
        '''
            sort by one of columns using ComputeGraph.sort
        '''
        col_count = 10
        row_count = 50
        maxint = 1000

        table = []
        records = []

        for i in range(row_count):
            record = {}
            row = []
            for j in range(col_count):
                value = randint(0, maxint)
                record[str(j)] = value
                row.append(value)
            records.append(record)
            table.append(row)

        ComputeGraphRunner.dump_to_file(records, "test_sort.txt")

        sort_by = 5

        sort_table = ComputeGraph("test_sort.txt") \
            .sort(str(sort_by))

        result = ComputeGraphRunner(sort_table).run()

        answer = sorted(table, key=itemgetter(sort_by))

        for row in range(row_count):
            for col in range(col_count):
                self.assertEqual(answer[row][col], result[row][str(col)])

if __name__ == '__main__':
    unittest.main()
