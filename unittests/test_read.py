import unittest

from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

class TestRead(unittest.TestCase):
    def test_read(self):
        '''
            test ComputeGraph.read
        '''
        records = []
        for i in range(50):
            record = {}
            for j in range(10):
                record[str(j)] = i + j
            records.append(record)

        ComputeGraphRunner.dump_to_file(records, "test_read.txt")

        graph = ComputeGraph("test_read.txt")

        result = ComputeGraphRunner(graph).run()

        for answer, my in zip(records, result):
            self.assertDictEqual(answer, my)

if __name__ == '__main__':
    unittest.main()
