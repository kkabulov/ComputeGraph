import unittest
from random import randint

from computegraph import ComputeGraph
from computegraphrunner import ComputeGraphRunner

def accumulate_folder(state, record):
    for column in state:
        state[column] += record[column]
    return state

class TestFold(unittest.TestCase):
    def test_fold(self):
        '''
            sum columns of a table using ComputeGraph.fold
        '''
        col_count = 10
        row_count = 50
        maxint = 1000

        answer = [0 for i in range(col_count)]

        records = []
        for i in range(row_count):
            dict = {}
            for j in range(col_count):
                value = randint(0, maxint)
                dict[str(j)] = value
                answer[j] += value
            records.append(dict)

        ComputeGraphRunner.dump_to_file(records, "test_fold.txt")

        columns_sum = ComputeGraph("test_fold.txt") \
            .fold(accumulate_folder, {str(i) : 0 for i in range(col_count)})

        result = ComputeGraphRunner(columns_sum).run()
        result = result[0]

        for i in range(col_count):
            self.assertEqual(answer[i], result[str(i)])

if __name__ == '__main__':
    unittest.main()
